﻿#########################################
############ Restaurant #################
#########################################
                Plovdiv Bulgaria
                ul."Georgi Carigradski 7"
                "Test Restaurant EOOD"
                EIK:27321789789
Waiter: Tester Waiter
Table number : 15
----------------------------------------
Fried potatoes
2x                        7,99$                        15,98$
Musaka
1x                        6,99$                         6,99$
Cola
5x                        0,99$                         4,95$
----------------------------------------
Discount: 0%
----------------------------------------
Total:                                                27,92$
----------------------------------------
You are always welcome in our
restaurant, hope everything was fine.
Manager name: Soft Academy
Manager phone: +359-99-99-99-99