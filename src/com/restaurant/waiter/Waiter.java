package com.restaurant.waiter;

import java.io.*;

public class Waiter {
    static String line;
    static boolean isFound = false;
    static final String WAITERS_TABLE_PATH = "/Users/Kalina/Desktop/Restaurant.AT/restaurant/DataBase/Waiter.txt";
    private String Name;

    public Waiter(String name) {
        this.Name = name;
    }

    public String getName() {
        return this.Name;
    }

    public static void checkWaiter(String InputWaiterID) throws IOException {
        File waiters = new File(WAITERS_TABLE_PATH);
        BufferedReader readWaiter = new BufferedReader(new FileReader(waiters));
        while ((line = readWaiter.readLine()) != null) {
            String[] waiterRow = line.split(",");
            if (waiterRow[0].equals(InputWaiterID)) {
                isFound = true;
                break;
            }
        }
        readWaiter.close();
        if (isFound == true) {
            System.out.println("Welcome to our Restaurant");
        } else {
            System.out.println("Incorrect ID try again!!!");
            System.exit(0);
        }
    }

    public static Waiter Names(String waiterID) throws IOException {
        String waiterName = null;
        File waiters = new File(WAITERS_TABLE_PATH);
        BufferedReader readWaiter = new BufferedReader(new FileReader(waiters));

        while ((line = readWaiter.readLine()) != null) {
            String[] waiterRow = line.split(",");
            if (waiterRow[0].equals(waiterID)) {
                waiterName = waiterRow[1];
                break;
            }
        }
        readWaiter.close();
        return new Waiter(waiterName);
    }
}



