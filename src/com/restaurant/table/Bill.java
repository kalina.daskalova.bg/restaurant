package com.restaurant.table;

import java.util.ArrayList;
import java.util.List;

public class Bill {
    private String waiterName;
    private int tableNumber;
    private List<MenuItem> orderedItems;
    private double discount;

    public Bill(int tableNumber, String waiterName) {
        this.tableNumber = tableNumber;
        this.waiterName = waiterName;
        this.orderedItems = new ArrayList<MenuItem>();
    }
    public void addOrderedItem(MenuItem item){
        orderedItems.add(item);
    }
    public void setDiscount(double discount){
        this.discount = discount;
    }
    public double calculationTotalAmount(){
        double total = 0;
        for (MenuItem item : orderedItems){
            total +=item.getPrice();
        }
        total -= discount;
        return total;
    }
    public void printBill(){
        System.out.println("Waiter: "+waiterName);
        System.out.println("Table number: "+ tableNumber);
        System.out.println("---------------------------");
        System.out.println("Ordered Items: ");
        for(MenuItem item: orderedItems){
            System.out.println(" -"+item.getName()+ ":"+ item.getPrice()+"$");
        }
        System.out.println("---------------------------");
        System.out.println("Discount: " + discount + "$");
        System.out.println("---------------------------");
        System.out.println("Total amount to pay: "+calculationTotalAmount()+ "$");
        System.out.println("---------------------------");
        System.out.println("\nYou are always welcome in our\n" +
                "restaurant, hope everything was fine.\n");

    }
}

