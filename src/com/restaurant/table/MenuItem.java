package com.restaurant.table;

public class MenuItem {
    private String Name;
    private double price;

    public MenuItem(String name, double price) {
        this.Name = name;
        this.price = price;
    }

    public String getName() {
        return Name;
    }

    public double getPrice() {
        return price;
    }
}
